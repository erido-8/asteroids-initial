 import greenfoot.*;

/**
 * A rocket that can be controlled by the arrowkeys: up, left, right.
 * The gun is fired by hitting the 'space' key. 'z' releases a proton wave.
 * 
 * @author Poul Henriksen
 * @author Michael Kölling
 * 
 * @version 1.1
 */
public class Rocket extends SmoothMover
{
    private static final int gunReloadTime = 5;         // The minimum delay between firing the gun.

    private int reloadDelayCount;               // How long ago we fired the gun the last time.
    
    private static final int waveReloadTime =120;
    private int waveReloadCount;
    
    private GreenfootImage rocket = new GreenfootImage("rocket.png");    
    private GreenfootImage rocketWithThrust = new GreenfootImage("rocketWithThrust.png");

    /**
     * Initialise this rocket.
     */
    public Rocket()
    {
        reloadDelayCount = 5;
        waveReloadCount = 30;
    }

    /**
     * Do what a rocket's gotta do. (Which is: mostly flying about, and turning,
     * accelerating and shooting when the right keys are pressed.)
     */
    public void act()
    {
        checkKeys();
        reloadDelayCount++;
        waveReloadCount++;
        move();
        checkAsteroidHit();
    }
    private void checkAsteroidHit()
    {
     Asteroid asteroid = (Asteroid) getOneIntersectingObject(Asteroid.class);
     if (asteroid != null)
     {
         Explosion explosion = new Explosion();
         getWorld().addObject(explosion,getX(),getY());
         getWorld().removeObject(this);
     }
    }
    /**
     * Check whether there are any key pressed and react to them.
     */
    private void checkKeys() 
    {
        if (Greenfoot.isKeyDown("space")) 
        {
            fire();
        }
        if(Greenfoot.isKeyDown("left")){
            turn(-5);
        }
        if(Greenfoot.isKeyDown("right")){
            turn(5);
        }
        if(Greenfoot.isKeyDown("up")){
            int degree = getRotation();
            Vector v =new Vector(degree,1.0);
            addToVelocity(v);
            animation();
        }else{
            setImage(rocket);
        }
        if(Greenfoot.isKeyDown("down")){
            int degree = getRotation();
            Vector v =new Vector(degree,-1.0);
            addToVelocity(v);
        }
        if(Greenfoot.isKeyDown("r")){
            fireProtonWave();
        }
    }
    /**
     * Fire a bullet if the gun is ready.
     */
    private void fire() 
    {
        if (reloadDelayCount >= gunReloadTime) 
        {
            Bullet bullet = new Bullet (getVelocity(),  getRotation());
            getWorld().addObject (bullet, getX(), getY());
            bullet.move ();
            reloadDelayCount = 0;
        }
    }
    public void fireProtonWave() {
        if(waveReloadCount >= waveReloadTime){
        ProtonWave pw = new ProtonWave();
        getWorld().addObject(pw, getX(), getY());
        waveReloadCount = 0;
        }
    }
    public void animation(){
        if(getImage()==rocket){
            setImage(rocketWithThrust);
        }       
    }
    
}